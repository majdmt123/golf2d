using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [SerializeField] private AudioClip clickButtonSound, explosionSound, rewardSound, backsound;

    private AudioSource audioSource;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            audioSource = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AudioManager>();

                if (instance == null)
                {
                    GameObject singletonObject = new GameObject("AudioManager");
                    instance = singletonObject.AddComponent<AudioManager>();
                }
            }

            return instance;
        }
    }

    public void PlayClickButtonSound()
    {
        PlaySound(clickButtonSound);
    }

    public void PlayExplosionSound()
    {
        PlaySound(explosionSound);
    }

    public void PlayRewardSound()
    {
        PlaySound(rewardSound);
    }

    public void PlayBacksound()
    {
        PlaySound(backsound);
    }

    private void PlaySound(AudioClip sound)
    {
        if (sound != null)
        {
            audioSource.PlayOneShot(sound);
        }
        else
        {
            Debug.LogWarning("Trying to play a null sound.");
        }
    }

}

