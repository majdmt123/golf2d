using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Pool;

public class LevelManager : MonoBehaviour
{
    public static LevelManager main;

    [Header("References")]
    [SerializeField] private TextMeshProUGUI strokeUI;
    [Space(10)]
    [SerializeField] private GameObject levelCompleteUI;
    [SerializeField] private TextMeshProUGUI levelCompletedStrokeUI;
    [Space(10)]
    [SerializeField] private GameObject gameOverUI;

    [Header("Attributes")]
    [SerializeField] private int maxStrokes;
    [SerializeField] private int levelUnlock;

    private int strokes;
    [HideInInspector] public bool outOfStrokes;
    [HideInInspector] public bool levelCompleted;

    private void Awake()
    {
        main = this;
    }

    private void Start()
    {
        UpdateStrokeUI();
    }

    public void IncreaseStroke()
    {
        strokes++;
        UpdateStrokeUI();

        if(strokes >= maxStrokes)
        {
            outOfStrokes = true;
        }
    }

    public void LevelComplete()
    {
        levelCompleted = true;

        levelCompletedStrokeUI.text = strokes > 1 ? "You putted in " + strokes.ToString() + " strokes" : "You got a hole in one!";

        levelCompleteUI.SetActive(true);

        int levelUnlocked = PlayerPrefs.GetInt("UnlockedLevel", 1);
        if(levelUnlocked <= levelUnlock)
        {
            PlayerPrefs.SetInt("UnlockedLevel", levelUnlock);
        }
    }

    public void GameOver()
    {
        gameOverUI.SetActive(true);
    }

    private void UpdateStrokeUI()
    {
        strokeUI.text = strokes.ToString() + "/" + maxStrokes.ToString();
    }
}
