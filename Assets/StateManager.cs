using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateManager : MonoBehaviour
{
    public void LoadLevel(string levelName)
    {
        AudioManager.instance.PlayClickButtonSound();
        SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Single);
    }
    public void ReloadLevel()
    {
        AudioManager.instance.PlayClickButtonSound();
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }
}
