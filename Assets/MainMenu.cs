using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject menuObjectsUI;
    [SerializeField] private GameObject levelMenuUI;

    public void PlayGame()
    {
        mainMenuUI.SetActive(false);
        menuObjectsUI.SetActive(false);
        levelMenuUI.SetActive(true);
        AudioManager.instance.PlayClickButtonSound();
    }

    public void BackMenu()
    {
        mainMenuUI.SetActive(true);
        menuObjectsUI.SetActive(true);
        levelMenuUI.SetActive(false);
        AudioManager.instance.PlayClickButtonSound();
    }

    public void ExitGame()
    {
        AudioManager.instance.PlayClickButtonSound();
        Application.Quit();
    }
}
